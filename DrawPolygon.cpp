#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3];
	int y[3];
	float phi = M_PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 3;
	}
	for(int i = 0; i < 3; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1)%3],ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	float phi = M_PI / 2;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi +=  M_PI / 2;
	}
	for (int i = 0; i < 4; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6];
	int y[6];
	float phi = M_PI / 2;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi +=  M_PI / 3;
	}
	for (int i = 0; i < 6; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}


void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R* cos(phi) + 0.5);
		y[i] = yc - int(R *sin(phi) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xnho[5];
	int y[5], ynho[5];
	float phi = M_PI / 2;
	float r = R * sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xnho[i] = xc + int(r*cos(phi + M_PI / 5) + 0.5);
		ynho[i] = yc - int(r*sin(phi + M_PI / 5) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xnho[i], ynho[i], ren);
		DDA_Line(xnho[i], ynho[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], xnho[8];
	int y[8], ynho[8];
	float phi = M_PI / 2;
	float r = R * sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xnho[i] = xc + int(r*cos(phi + M_PI / 5) + 0.5);
		ynho[i] = yc - int(r*sin(phi + M_PI / 5) + 0.5);
		phi += 2 * M_PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawPoint(ren, x[i], y[i]);
	}
	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xnho[i], ynho[i], ren);
		DDA_Line(xnho[i], ynho[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{

}
