#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0, y = 0, p=A, p2 = 2 * p, p4 = 2 * p2, d = 1 - p;
	while (x < p && y <= 600)
	{
		Draw2Points(xc, yc,x,y,ren);
		if (d >= 0)
		{
			y++;
			d -= p2;
		}
		x++; d = d + 2 * x + 1;
	}
	if (d == 1)
		d = 1 - p4;
	else
		d = 1 - p2;
	while (y <= 600)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (d <= 0)
		{
			x++;
			d += 4*x;
		}
		y++;
		d -= p4;
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p = -A, x = 0, y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A && y >= -600)
	{

		if (p > 0)
		{
			p = p - (2 * x + 1);
		}
		else
		{
			p = p + 2 * A - (2 * x + 1);
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2*x*x - 2*x - 1 + 4*A*y;
	while ((x > A) && (y >= -600)) 
	{
		if (p > 0)
		{
			p = p - 4 * A;
		}
		else
		{
			p = p + 4*x + 4 - 4*A;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}

}
